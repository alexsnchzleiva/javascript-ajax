<?php
	
    $conex = new mysqli("127.0.0.1","root","root","ajax");
	$query = "SELECT * FROM paises";
    $result = $conex->query($query);
	
	$xml  = '<?xml version="1.0" standalone="yes"?>';
	$xml .= '<datos>';	
		while($row = $result->fetch_array()){
			$xml .= '<pais>';
				$xml .= '<nombre>';
					$xml .= $row['pais'];
				$xml .= '</nombre>';
				$xml .= '<extension>';
					$xml .= rand (10000, 1000000);
				$xml .= '</extension>';
				$xml .= '<habitantes>';
					$xml .= rand (1, 10);
				$xml .= '</habitantes>';
			$xml .= '</pais>';
		}	
		$xml.= '<numero_resultados>'.$result->num_rows.'</numero_resultados>';
	$xml .= '</datos>';
	header('Content-type: text/xml');
	
	echo $xml;
			
?>